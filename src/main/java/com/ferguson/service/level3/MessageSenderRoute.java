package com.ferguson.service.level3;

import java.net.HttpURLConnection;

import javax.ws.rs.core.MediaType;

import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.builder.xml.XPathBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.bean.validator.BeanValidationException;
import org.apache.camel.model.dataformat.JsonLibrary;

@ContextName("com.ferguson.service.level3")
public class MessageSenderRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        onException(BeanValidationException.class).id("Bean Validation Excp").handled(true)
                .process("beanValidationExceptionProcessor").id("Bean Validation Error Processor").choice()
                .when(header("accept").isEqualTo("application/xml")).marshal().jacksonxml().endChoice().otherwise()
                .marshal().json(JsonLibrary.Jackson);
				

        from(ConstantsRest.ROUTE_ID_SENDAMQMESSAGE).routeId("SendMessagedddd").log("queueName : ${headers.address}")
                .toD("direct:${headers.address}").log("transid : ${headers.transid}").choice()
                .when(header("filter").isEqualTo("testing"))
                .setHeader("testing", XPathBuilder.xpath("true", String.class)).end()
                .setExchangePattern(ExchangePattern.InOnly)
                .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_JSON))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpURLConnection.HTTP_OK));
    }
}
