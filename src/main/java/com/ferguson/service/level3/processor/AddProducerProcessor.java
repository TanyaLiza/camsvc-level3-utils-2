package com.ferguson.service.level3.processor;

import java.io.IOException;

import javax.inject.Named;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.ferguson.service.common.camel.CommonConstants;
import com.ferguson.service.level3.Constants;
import com.ferguson.service.level3.vo.AddProducerRequest;

@Named
public class AddProducerProcessor implements Processor {

	private static final Logger LOG = Logger.getLogger(AddProducerProcessor.class);

	public void checkTransId(Exchange exchange) throws JsonParseException, JsonMappingException, IOException {
		String transId = null;
		if (exchange.getIn().getBody() != null) {
			final AddProducerRequest request = exchange.getIn().getBody(AddProducerRequest.class);
			LOG.debug("Add Producer Request " + request);
			if (request != null && request.getTransId() != null) {
				transId = request.getTransId();
			}
			if (request != null && request.getIsTopic().equals("true")) {
				exchange.getIn().setHeader(Constants.ACTIVEMQ, "activemq:topic:");
			} else {
				exchange.getIn().setHeader(Constants.ACTIVEMQ, "activemq:queue:");
			}

			if (transId != null && !transId.trim().isEmpty()) {
				exchange.setProperty(CommonConstants.FEI_TRANS_ID_PROP, transId);
			}
			exchange.getIn().setHeader(Constants.ROUTE_ID, request.getAddress());

		}
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		String address = exchange.getIn().getHeader("route.id", String.class);
		String mq = exchange.getIn().getHeader(Constants.ACTIVEMQ, String.class);
		DynamicRouteBuilder route = new DynamicRouteBuilder(address, mq);
		exchange.getContext().addRoutes(route);

	}

	private static final class DynamicRouteBuilder extends RouteBuilder {

		String address = null;
		String activemq = null;

		private DynamicRouteBuilder(String address, String mq) {
			this.address = address;
			this.activemq = mq;
		}

		@Override
		public void configure() throws Exception {
			from("direct:" + address).routeId("producer-" + address).log("Send message =" + activemq + address)
					.toD(activemq + address);
		}
	}
}
