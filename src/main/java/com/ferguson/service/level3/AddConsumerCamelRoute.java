package com.ferguson.service.level3;

import java.net.HttpURLConnection;

import javax.ws.rs.core.MediaType;

import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.bean.validator.BeanValidationException;
import org.apache.camel.model.dataformat.JsonLibrary;

import com.fasterxml.jackson.core.JsonParseException;
import com.ferguson.service.common.camel.CommonConstants;
import com.ferguson.service.common.camel.util.Helpers;
import com.ferguson.service.level3.processor.BadRequestException;
import com.ferguson.service.level3.vo.AddConsumerRequest;

@ContextName("com.ferguson.service.level3")
public class AddConsumerCamelRoute extends RouteBuilder {

	private static final Object TESTING = "testing";

	@Override
	public void configure() throws Exception {
		
		
		onException(BeanValidationException.class).handled(true)
		.setProperty(CommonConstants.FEI_RESP_CODE_PROP, constant(Constants.MANDATORY_VALIDATION_ERROR))
		.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpURLConnection.HTTP_BAD_REQUEST))
		.setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_JSON))
		.setProperty(CommonConstants.FEI_LOG_MESSAGE_PROP, constant(Helpers.translateMessageCode(Constants.MANDATORY_VALIDATION_ERROR)))
		.log(LoggingLevel.ERROR, Helpers.translateMessageCode(Constants.MANDATORY_VALIDATION_ERROR) + " >>> Payload :" + Constants.EXCHANGE_BODY)
        .log(LoggingLevel.ERROR, Constants.STACK_TRACE).bean("beanValidationResponseBuilder").marshal()
        .json(JsonLibrary.Jackson).end();
		
		
		onException(Throwable.class).handled(true)
		.setProperty(CommonConstants.FEI_RESP_CODE_PROP, constant(Constants.INTERNAL_ERROR))
        .setProperty(CommonConstants.FEI_LOG_MESSAGE_PROP, constant(Helpers.translateMessageCode(Constants.INTERNAL_ERROR)))
        .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_JSON))
        .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpURLConnection.HTTP_INTERNAL_ERROR))
        .log(LoggingLevel.ERROR, Constants.INTERNAL_ERROR + " >>> Payload :" + Constants.EXCHANGE_BODY)
        .log(LoggingLevel.ERROR, Constants.STACK_TRACE).bean("responseBuilder").marshal()
        .json(JsonLibrary.Jackson).end();
        
		from("direct:addConsumer")
		.process(new Processor() {
			@Override
			public void process(Exchange exchange) throws Exception {
				AddConsumerRequest request = exchange.getIn().getBody(AddConsumerRequest.class);
				if (null != request.getTransId()) {
					exchange.setProperty(CommonConstants.FEI_TRANS_ID_PROP, request.getTransId());
				}

			}
		})
		.to("bean-validator://requestValidator")		
		.process(new Processor() {
			@Override
			public void process(Exchange exchange) throws Exception {
				AddConsumerRequest request = exchange.getIn().getBody(AddConsumerRequest.class);
				ConsumeMessageRoute route = new ConsumeMessageRoute(request.getQueue(), request.getFilter());
				getContext().addRoutes(route);
			}
		}).id("Add_Consumer_Route")
		.setExchangePattern(ExchangePattern.InOnly)
        .setProperty(CommonConstants.FEI_LOG_MESSAGE_PROP, constant(Helpers.translateMessageCode(Constants.CONSUMER_REG_SUCCESS)))
        .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_JSON))
        .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpURLConnection.HTTP_OK))
        .log(LoggingLevel.INFO,  Helpers.translateMessageCode(Constants.CONSUMER_REG_SUCCESS))
        .bean("responseBuilder");
	}

	private static final class ConsumeMessageRoute extends RouteBuilder {

		private String queueName = null;
		private String selector =  "";

		private ConsumeMessageRoute(final String queueName, final String filter) {
			this.queueName = queueName;
			if(null != filter && filter.length() > 0) {
				selector = "?selector="+filter+"='true'";
			}
		}

		@Override
		public void configure() throws Exception {
			from("activemq:queue:" + queueName+selector).id(queueName).log("Reading message from Queue   : Body is: ${body} ");
		}
	}
}
