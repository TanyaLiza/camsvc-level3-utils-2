package com.ferguson.service.level3;

import java.net.HttpURLConnection;

import javax.ws.rs.core.MediaType;

import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.model.dataformat.JsonLibrary;

import com.fasterxml.jackson.core.JsonParseException;
import com.ferguson.service.common.camel.CommonConstants;
import com.ferguson.service.common.camel.util.Helpers;
import com.ferguson.service.level3.processor.BadRequestException;
import com.ferguson.service.level3.vo.AddConsumerRequest;

@ContextName("com.ferguson.service.level3")
public class RemoveConsumerCamelRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		onException(BadRequestException.class).handled(true)
		.setProperty(CommonConstants.FEI_RESP_CODE_PROP, constant(Constants.MANDATORY_VALIDATION_ERROR))
		.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpURLConnection.HTTP_BAD_REQUEST))
		.setProperty(CommonConstants.FEI_LOG_MESSAGE_PROP, constant(Helpers.translateMessageCode(Constants.MANDATORY_VALIDATION_ERROR)))
		.log(LoggingLevel.ERROR, constant(Helpers.translateMessageCode(Constants.MANDATORY_VALIDATION_ERROR)) + " >>> Payload :" + Constants.EXCHANGE_BODY)
        .log(LoggingLevel.ERROR, Constants.STACK_TRACE).bean("responseBuilder").marshal()
        .json(JsonLibrary.Jackson).end();
		
		onException(Throwable.class).handled(true)
		.setProperty(CommonConstants.FEI_RESP_CODE_PROP, constant(Constants.INTERNAL_ERROR))
        .setProperty(CommonConstants.FEI_LOG_MESSAGE_PROP, constant(Helpers.translateMessageCode(Constants.INTERNAL_ERROR)))
        .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_JSON))
        .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpURLConnection.HTTP_INTERNAL_ERROR))
        .log(LoggingLevel.ERROR, Constants.INTERNAL_ERROR + " >>> Payload :" + Constants.EXCHANGE_BODY)
        .log(LoggingLevel.ERROR, Constants.STACK_TRACE).bean("responseBuilder").marshal()
        .json(JsonLibrary.Jackson).end();
		
		from("direct:deleteConsumer")
		.process(new Processor() {
			@Override
			public void process(Exchange exchange) throws Exception {
				if(null == exchange.getIn().getHeader("queue")) {
					throw new BadRequestException(Helpers.translateMessageCode(Constants.BAD_REQUEST, "There is no request"));
				}
				String routeId = exchange.getIn().getHeader("queue").toString();
				getContext().stopRoute(routeId);
				getContext().removeRoute(routeId);
			}
		}).id("Delete_Consumer_Route")
		.setExchangePattern(ExchangePattern.InOnly)
        .setProperty(CommonConstants.FEI_LOG_MESSAGE_PROP, constant(Helpers.translateMessageCode(Constants.CONSUMER_REMOVE_SUCCESS)))
        .setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_JSON))
        .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpURLConnection.HTTP_OK))
        .log(LoggingLevel.INFO,  Helpers.translateMessageCode(Constants.CONSUMER_REMOVE_SUCCESS))
        .bean("responseBuilder");
	}

}
