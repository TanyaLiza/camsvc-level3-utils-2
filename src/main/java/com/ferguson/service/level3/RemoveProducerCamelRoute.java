package com.ferguson.service.level3;

import java.net.HttpURLConnection;

import javax.ws.rs.core.MediaType;

import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.model.dataformat.JsonLibrary;

import com.ferguson.service.common.camel.CommonConstants;
import com.ferguson.service.common.camel.util.Helpers;
import com.ferguson.service.level3.processor.BadRequestException;

@ContextName("com.ferguson.service.level3")
public class RemoveProducerCamelRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		onException(BadRequestException.class).handled(true)
				.setProperty(CommonConstants.FEI_RESP_CODE_PROP, constant(Constants.MANDATORY_VALIDATION_ERROR))
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpURLConnection.HTTP_BAD_REQUEST))
				.setProperty(CommonConstants.FEI_LOG_MESSAGE_PROP,
						constant(Helpers.translateMessageCode(Constants.MANDATORY_VALIDATION_ERROR)))

				.log(LoggingLevel.ERROR,
						constant(Helpers.translateMessageCode(Constants.MANDATORY_VALIDATION_ERROR)) + " >>> Payload :"
								+ Constants.EXCHANGE_BODY)
				.log(LoggingLevel.ERROR, Constants.STACK_TRACE).bean("removeProducerProcessor").marshal()
				.json(JsonLibrary.Jackson).end();

		from(ConstantsRest.ROUTE_ID_DELETEAMQPRODUCER).routeId("removeAmqProducer")

				.process(new Processor() {
					@Override
					public void process(Exchange exchange) throws Exception {
						if (exchange.getIn().getHeader("transId") != null ) {
							exchange.setProperty(CommonConstants.FEI_TRANS_ID_PROP, exchange.getIn().getHeader("transId"));
						}
						if (null == exchange.getIn().getHeader("address")) {
							throw new BadRequestException(Helpers.translateMessageCode(Constants.BAD_REQUEST,
									"remove.amq.producer.invalid.address"));
						}
						
						String route_Id = exchange.getIn().getHeader("address", String.class);
						getContext().stopRoute(route_Id);
						getContext().removeRoute(route_Id);
					}
				}).id("Removed a Publisher to topic/queue").log("Removing address : ${headers.route.id}")
				.setExchangePattern(ExchangePattern.OutOnly)
				.setProperty(CommonConstants.FEI_LOG_MESSAGE_PROP,
						simple("Successfully removed Producer for amq address: ${headers.address}"))
				.bean("responseBuilder").setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_JSON))
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpURLConnection.HTTP_OK));
	}

}
